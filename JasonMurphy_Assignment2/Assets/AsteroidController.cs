﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AsteroidController : NetworkBehaviour
{
    Vector3 velocity, acceleration, axisOfRotation;
    float speedOfRotation;
    [SyncVar]
    float health = 100;
    DamageController damage;
    public Transform laserSmoke;

    // Use this for initialization
    void Start()
    {
        damage = GetComponent<DamageController>();
        velocity = new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10));
        speedOfRotation = Random.Range(-50, 50);
        axisOfRotation = new Vector3( Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10));
        axisOfRotation.Normalize();

    }

    // Update is called once per frame
    void Update()
    {
        // if is not the server return 
        if (!isServer) return;

        // if is the server apply the rotation and transform.
        transform.Rotate( axisOfRotation,speedOfRotation * Time.deltaTime);
        velocity += acceleration * Time.deltaTime;
        transform.position += velocity * Time.deltaTime;
    }

   
    void OnCollisionEnter(Collision col)
    {
        // if collide with bullet take damage
        if(col.gameObject.tag == "Bullet")
        {
            damage.RegiterHit(20.0f,true);
        }
        // if collide with player take damage
        if (col.gameObject.tag == "Player")
        {
            // Create laserSmoke particle effect from prefab on collision
            Transform particle = (Transform)Instantiate(laserSmoke, col.gameObject.transform.position, Quaternion.identity);
            damage.RegiterHit(1000,true);
        }
    }
}
