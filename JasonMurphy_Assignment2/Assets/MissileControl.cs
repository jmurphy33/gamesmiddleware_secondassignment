﻿using UnityEngine;
using System.Collections;

public class MissileControl : MonoBehaviour {

    Vector3 velocity;
    Vector3 acceleration;
    private float ThrustValue = 100.0f;
    Transform TheTarget;
	// Use this for initialization
	void Start () {


	
	}
	
	// Update is called once per frame
	void Update () {
        acceleration = Vector3.zero;
        transform.rotation = Quaternion.LookRotation(TheTarget.position - transform.position);
        acceleration += ThrustValue * transform.forward;
        velocity += acceleration * Time.deltaTime;

        transform.position +=
            velocity * Time.deltaTime;


	
	}

    internal void StartUpInfo(Vector3 startUpVelocity, Transform target)
    {
        velocity = startUpVelocity;
        TheTarget = target;
    }

    void OnCollisionEnter(Collision col)
    {

        print("Hit with Missile");
    }
}
