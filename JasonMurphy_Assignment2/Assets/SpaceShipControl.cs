using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class SpaceShipControl : NetworkBehaviour
{

    public Transform laserSmoke;

    public Vector3 velocity, acceleration;

    [SyncVar]
    LineRenderer ourLaser;

    float thrustValue = 10;
  
    float maxRotationalSpeed = 360;
 
    private float laserEnergy = 100;
    private float depletionRate = 20;
    private float repletionRate = 5;
    private float MaxLineRenderForLaserDistance = 1000;
    private float airResistanceFactor = 0.1f;

    public Camera cam;


    DamageController damage;

    public float Energy
    {
        get
        {
            return laserEnergy;
        }

        set
        {
            if (value > 100)
            {
                laserEnergy = 100;
            }
            else
            {
                if (value < 0) laserEnergy = 0;
                else laserEnergy = value;
            }

        }
    }


    void OnGUI()
    {
        if (!isLocalPlayer) return;
        else
        {
            GUI.Box(new Rect(Screen.width - 100, 0, 100, 50), "");
            GUI.Label(new Rect(Screen.width - 100, 0, 100, 50), "Health: " + damage.health);
            GUI.Label(new Rect(Screen.width - 100, 15, 100, 50), "Laser: " + laserEnergy);
        }

    }

    // Use this for initialization
    void Start()
    {    
        damage = GetComponent<DamageController>();
        ourLaser = GetComponent<LineRenderer>();
        ourLaser.enabled = false;
        if (isLocalPlayer)
        {              
            return;
        }      
        else
        {        
            cam.enabled = false;
        }
        
    }

    // Update is called once per frame


    void Update()
    {
        

        // if is not local player return 
        if (!isLocalPlayer) return;

        // if is the local player apply the usual functionality 

        // contact server with button press for firing
        CmdFireLaser2(Input.GetKey(KeyCode.Mouse0), laserEnergy);
  
        acceleration = Vector3.zero;
        Roll(Input.GetAxis("Horizontal"));
        Pitch(Input.GetAxis("Vertical"));

        // if space is press accelerate faster
        if (Input.GetButton("Jump")) { Thrust();}

        Energy += repletionRate * Time.deltaTime;

        if (Input.GetButton("Fire2")) { TryForMissileLock();}
                       
        // Apply velocity to position     
        acceleration += -airResistanceFactor * velocity;
        velocity += acceleration * Time.deltaTime;
        transform.position += velocity * Time.deltaTime;
    }



    private void TryForMissileLock()
    {
        GameObject[] asteroids = GameObject.FindGameObjectsWithTag("Asteroid");
        GameObject theAsteroid = null;

        foreach (GameObject asteroid in asteroids)
        {
            float angle = angleBetween(asteroid.transform, transform);

            if (angle < 2 * Mathf.Deg2Rad)
            {
                theAsteroid = asteroid;
            }
        }

        if (theAsteroid)
        {
            theAsteroid.GetComponent<MissileLockOn>().MissileLockHit(this);
        }


    }

    private float angleBetween(Transform asteroid, Transform spaceship)
    {
        Vector3 SpaceShiptoAsteroid = asteroid.position - spaceship.position;
        float cosAngle = Vector3.Dot(spaceship.forward, SpaceShiptoAsteroid)
            / (SpaceShiptoAsteroid.magnitude * spaceship.forward.magnitude);
        return Mathf.Acos(cosAngle);

    }


    /// <summary>
    /// Applies the laser fire to all clients
    /// </summary>
    /// <param name="buttonDown"></param>
    [ClientRpc]
    public void RpcFireLaser2(float laserEnergy)
    {
     
        // turn on laser
        ourLaser.enabled = true;

        // if energy is there
        if (laserEnergy > 0)
        {
            // take away energy to stop constant firing
            Energy -= depletionRate * Time.deltaTime;

            // create ray to show laser
            Ray laser = new Ray(transform.position, transform.forward);

            RaycastHit hit;

            // if the laser hits something
            if (Physics.Raycast(laser, out hit))
            {
                // print the object hit
                print("Hit: " + hit.collider.gameObject.tag);
                
                // if the object hit is an asteroid  
                if (hit.collider.gameObject.tag == "Asteroid")
                {
                    // create the impact point of where the hit happened
                    Vector3 impactPoint = new Vector3(0, 0, hit.distance / transform.localScale.x);

                    // take health away from the asteroid. True means destroy the object if health hits 0.
                    // Since called from each client all are consistent. The registerHit method doesn't need to be RPC/CMD
                    hit.collider.gameObject.GetComponent<DamageController>().RegiterHit(10.0f, true);

                    // create particle effect from laserSmoke prefeb on the point the imact occured with zero rotation.
                    Transform particle = (Transform)Instantiate(laserSmoke, hit.point, Quaternion.identity);

                    particle.parent = hit.transform;
                }
                // if the laser hits a player object
                else
                {
                    // create the impact point of where the hit happened
                    Vector3 impactPoint = new Vector3(0, 0, hit.distance / transform.localScale.x);

                    // create particle effect from laserSmoke prefeb on the point the imact occured with zero rotation.
                    Transform particle = (Transform)Instantiate(laserSmoke, hit.point, Quaternion.identity);

                    particle.parent = hit.transform;

                    // take health away from the other player. False means don't destroy the object if health hits 0, but respawn instead.
                    hit.collider.gameObject.GetComponent<DamageController>().RegiterHit(10.0f, false);

                }
            }
        }
        else
        {
            Vector3 impactPoint = new Vector3(0, 0, MaxLineRenderForLaserDistance);
            ourLaser.SetPosition(1, impactPoint);
            print("not hitting");
        }
    }


    /// <summary>
    /// Server command to check if button is pressed
    /// If the button is pressed distribute the coordinates of
    /// the laster/damage to each client
    /// </summary>
    /// <param name="buttonDown"></param>
    [Command]
    public void CmdFireLaser2(bool buttonDown, float laserEnergy )
    {
        // if the button is pressed by the client
        if (buttonDown)
        {
            // apply the code for each client, called from server. 
            RpcFireLaser2(laserEnergy);
        }
        // if not pressed turn off laser on each client
        else RpcStopLaser2();
    }

    /// <summary>
    /// Client will turn off the laser if not pressed
    /// </summary>


    [ClientRpc]
    public void RpcStopLaser2()
    {
        try
        {
           ourLaser.enabled = false;
        }
        catch(Exception e)
        {

        }
    }

    

    void Roll(float dir)
    {
        transform.Rotate(Vector3.forward, maxRotationalSpeed * dir * Time.deltaTime);

    }

    void Pitch(float dir)
    {
        transform.Rotate(Vector3.right, maxRotationalSpeed * dir * Time.deltaTime);

    }

    void Thrust()
    {
        acceleration += transform.forward * thrustValue;
    }

   
}
