﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Not used. 
/// </summary>

public class BulltetController : MonoBehaviour {
    public Transform laserSmoke;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision col)
    {
        Destroy(this.gameObject);
        if (col.gameObject.tag == "Player")
        {      
            print("Player hit...");
            col.gameObject.GetComponent<DamageController>().RegiterHit(10,false);           
        }
        if (col.gameObject.tag == "Asteroid")
        {     
            print("Asteroid hit...");
            col.gameObject.GetComponent<DamageController>().RegiterHit(10, true);
        }
    }
}
