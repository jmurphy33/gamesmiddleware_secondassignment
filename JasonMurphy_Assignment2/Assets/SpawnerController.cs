﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class SpawnerController : NetworkBehaviour {

    [SyncVar]
    public GameObject asteroid;
    int numberOfAsteroids = 100;
    float worldBound = 100;
	
    /// <summary>
    /// When the server starts add asteroids for everyone.
    /// </summary>
    public override void OnStartServer()
    {
        for (int i = 0; i < numberOfAsteroids; i++)
        {
            var ast = (GameObject)Instantiate(asteroid, new Vector3(Random.Range(-worldBound, worldBound),
                                                Random.Range(-worldBound, worldBound),
                                                Random.Range(-worldBound, worldBound)),
                                                Quaternion.identity);
            NetworkServer.Spawn(ast);

        }
    }

}
