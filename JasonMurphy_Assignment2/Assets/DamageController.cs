﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class DamageController : NetworkBehaviour {
    [SyncVar]
    public float health = 10000;
	// Use this for initialization
	void Start () {
	
	}
	
    void OnGui()
    {
        GUI.Box(new Rect(Screen.width - 100, 0, 100, 50), "Loader Menu");
        GUI.Label(new Rect(0, 0, 100, 50), ""+health);

    }

    // Update is called once per frame
    void Update () {
	
	}

    /// <summary>
    /// Called from ClientRPC, so this method doesn't need to be RPC/CMD
    /// Applied on each client.
    /// </summary>
    /// <param name="damageAmount"></param>
    /// <param name="destroy"></param>
    public void RegiterHit(float damageAmount, bool destroy)
    {
        // take health away using the damage parameter 
        health -= damageAmount;
       
        // if health is less than 0 and want to destory
        // used for asteroids after reaching 0 health
        if (health < 0 && destroy) { Destroy(this.gameObject);}

        // if health is less than 0 and don't want to destroy, apply respawn to starting position.
        // used for spaceships after reaching 0 health
        else if (health <= 0 && !destroy)
        {
            this.GetComponent<SpaceShipControl>().transform.position = Vector3.zero;
            this.GetComponent<SpaceShipControl>().acceleration = Vector3.zero;
            this.GetComponent<SpaceShipControl>().velocity = Vector3.zero;
            this.GetComponent<SpaceShipControl>().transform.rotation = Quaternion.identity;
            health = 10000;
        }
        
    }
}
