﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CameraControl : NetworkBehaviour {

	float distanceBehindShip = 30f;
	float distanceAboveShip = 15;
	float distancetoFocus = 200;

	GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		if (!player)
						print ("Player not found"); 
	}
	
	// Update is called once per frame
	void Update () {
        if (!base.isLocalPlayer) return;
		 transform.position = Vector3.Lerp(transform.position, 
                                    player.transform.position 
								- distanceBehindShip * player.transform.forward 
									+ distanceAboveShip * player.transform.up,
                                    0.2f);


        Vector3 target = player.transform.position 
							+ distancetoFocus * player.transform.forward;
		transform.rotation = Quaternion.Slerp(
            transform.rotation,
            Quaternion.LookRotation (
									target - transform.position, 
									player.transform.up), 
            0.2f);



	
	}
}
