﻿using UnityEngine;
using System.Collections;

public class TextControlForMissileLock : MonoBehaviour {

    float maxAngletoContinueMissileLock = 30.0f * Mathf.Deg2Rad;
    float lockTimer = 0;
    float TimeToAchieveLock = 5.0f;
    SpaceShipControl shipWithLockOn;
	// Use this for initialization
	void Start () {

  
	
	}
	
	// Update is called once per frame
	void Update () {
      
	}

    private bool angleIssmallEnough(TextControlForMissileLock textControlForMissileLock, SpaceShipControl shipWithLockOn)
    {
        return angleBetween(transform.parent, shipWithLockOn.transform) < maxAngletoContinueMissileLock;
    }

    private float angleBetween(Transform asteroid,Transform spaceship)
    {
        Vector3 SpaceShiptoAsteroid =
                        asteroid.position - spaceship.position;
        float cosAngle =
            Vector3.Dot(spaceship.forward, SpaceShiptoAsteroid)
            / (SpaceShiptoAsteroid.magnitude * spaceship.forward.magnitude);
        return Mathf.Acos(cosAngle);

    }

    internal void LockAchieved(SpaceShipControl spaceShipControl)
    {
        shipWithLockOn = spaceShipControl ;
    }
}
