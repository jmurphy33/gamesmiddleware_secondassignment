﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SpaceManager : NetworkManager
{

 
    /// <summary>
    /// Assosiates a player object with an ID when they are added to the game.
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="playerControllerId"></param>
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        Vector3 spawnPos = Vector3.right * conn.connectionId;
        GameObject player = (GameObject)Instantiate(base.playerPrefab, spawnPos, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }

    /// <summary>
    /// Starts host
    /// </summary>
    public void StartGame()
    {
        NetworkManager.singleton.StartHost();
    }
    
    /// <summary>
    /// Uses port/ip to allow player to join
    /// </summary>
    public void JoinGame()
    {
        NetworkManager.singleton.StartClient();
    }
}