﻿using UnityEngine;
using System.Collections;

public class MissileLockOn : MonoBehaviour {
    TextControlForMissileLock text;

	// Use this for initialization
	void Start () {
        text =
            GetComponentInChildren<TextControlForMissileLock>();
        text.gameObject.SetActive(false);

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void MissileLockHit(SpaceShipControl spaceShipControl)
    {
        text.gameObject.SetActive(true);
        text.LockAchieved(spaceShipControl);
    }
}
